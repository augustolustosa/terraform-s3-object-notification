variable "name" {
  type        = string
  description = ""
}

variable "tags" {
  type        = map(string)
  description = "bucket tags"
  default     = {}
}

variable "policy" {
  type        = string
  description = "policy var"
  default     = ""
}

variable "acl" {
  type        = string
  description = "acl var"
  default     = "private"
}

variable "force_destroy" {
  type        = bool
  description = "destroy bucket force"
  default     = false
}

variable "versioning" {
  description = "Map containing versioning configuration."
  type        = map(string)
  default     = {}
}

variable "logging" {
  description = "Map containing logging configuration."
  type        = map(string)
  default     = {}
}

variable "website" {
  description = "Map containing website configuration."
  type        = map(string)
  default     = {}
}

variable "filepath" {
  description = "local onde os arquivos vao ficar"
  type        = string
  default     = ""
}

variable "key_prefix" {
  description = "prefixo para colocar minhas keys"
  type        = string
  default     = ""
}